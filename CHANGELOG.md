## [1.0.1](https://gitlab.ics.muni.cz/perun/semantic-release-perun-config/compare/v1.0.0...v1.0.1) (2024-01-17)


### Bug Fixes

* remove dependency on itself ([2edd241](https://gitlab.ics.muni.cz/perun/semantic-release-perun-config/commit/2edd24121b5c0c9ee4f6b5c708b0ec99a99f4df2))

# 1.0.0 (2024-01-17)


### Features

* initial commit ([6852fc9](https://gitlab.ics.muni.cz/perun/semantic-release-perun-config/commit/6852fc9803a89a78c583628a3a4955f858c310f3))
